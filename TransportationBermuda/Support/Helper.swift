//
//  Helper.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-17.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import Foundation

let routeArray = ["Pink route", "Blue route", "Green route", "Orange route"]
var selected = 0

let stationsPink = ["leave Hamilton", "Lower Ferry", "Hodsdon's Ferry", "Salt Kettle", "Darrell's Wharf", "Belmont Ferry", "Hinson's Island"]
let stationsBlue = ["Leave Hamilton","Dockyard"]
let stationsGreen = ["Leave Hamilton","Watford Bridge","Cavello Bay", "Rockaway"]
let stationsOrange = ["Leave Hamilton","St Georges"]

enum Route: String {
    case pink = "Pink route"
    case blue = "Blue route"
    case green = "Green route"
    case orange = "Orange route"
}





