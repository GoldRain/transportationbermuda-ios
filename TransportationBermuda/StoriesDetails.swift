//
//  StoriesDetailsTableViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-31.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//


//import UIKit
//
//class RouteTimeTableViewCell: UITableViewCell {
//    @IBOutlet var nameLabel: UILabel!
//    @IBOutlet var timeLabel: UILabel!
//}
//
//class HeaderSection : UIView {
//    @IBOutlet var FerryLabel: UILabel!
//    @IBOutlet var dateLabel: UILabel!
//}
//    
//
//
//class StoriesDetails: UITableViewController {
//    
//    var route: RouteSchedule!
//    
//    var schedule: RouteTimes! { didSet { self.updateSchedule() }}
//    var days: [DaySchedule] = []
//    
//    func updateSchedule() {
//        self.tableView?.reloadData()
//        }
//    
////    
////    
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.schedule?.stops.count ?? 0
//    }
//
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
//
//    /*
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
// 
//        let headerView = tableView.dequeueReusableCell(withIdentifier: "headerSection") as! HeaderSection
// 
//        let  header = UIView(frame: CGRect(x: 20, y: 10, width: 200, height: 50))
//        let label = UILabel(frame: CGRect(x: 0, y: 12, width: header.bounds.width, height: 25))
//        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        label.font = UIFont (name: "TimesNewRomanPS-BoldMT", size: 25)
//        label.textColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 1.0)
//        label.text = schedule?.visibleTime //route?.title
//        label.textAlignment = .center
//        header.addSubview(label)
//        header.backgroundColor = route?.headerColor
// 
//        headerView.FerryLabel.text = "abc"
//        headerView.dateLabel.text = "cde"
// 
//        return headerView
//    }
//    */
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "RouteTimeTableViewCell", for: indexPath) as! RouteTimeTableViewCell
//        cell.nameLabel?.font = UIFont (name: "Times New Roman", size: 20)
//        cell.timeLabel?.font = UIFont (name: "Times New Roman", size: 20)
//        cell.nameLabel?.text = self.schedule.stops[indexPath.row].stop
//        cell.timeLabel?.text = self.schedule.stops[indexPath.row].time
//        //Label 2 : time
//    
//        return cell
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//    }
//
//
//
//}
