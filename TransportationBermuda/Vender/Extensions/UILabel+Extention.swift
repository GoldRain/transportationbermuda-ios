//
//  UILabel+Extention.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/24/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit

extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(
                NSAttributedString.Key.underlineStyle,
                value: NSUnderlineStyle.single.rawValue,
                range: NSRange(location: 0, length: attributedString.length)
            )
            attributedText = attributedString
        }
    }
}
