//
//  UIViewController+Extensions.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-12.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit

//extension UIViewController {
//    func showAlert(title: String, message: String) {
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//}


//setting up stackview
extension UIViewController {
    
    @IBAction func showFerryRoutesController() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FerryRoutesViewController") as! FerryRoutesViewController
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func showMapsController() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Maps") as! Maps
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    
    @IBAction func showInfoController() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Infosection") as! Infosection
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
        
    @IBAction func showFavoriteController() {
         let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Favorite") as! Favorite
         
         self.navigationController?.pushViewController(controller, animated: true)
    }
}
