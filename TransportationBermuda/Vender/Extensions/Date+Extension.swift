//
//  Date+Extension.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-02-21.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import Foundation

extension Date {
    enum ScheduleType { case weekday, saturday, sunday }
    
    var isWinter: Bool {
        let components = Calendar.current.dateComponents([.month, .day], from: self)
        let month = components.month ?? 1
        let day = components.day ?? 1
        
        if month < 4 { return true }
        if month == 4 { return day < 13 }
        
        if month == 12 { return true }
        if month == 11 { return day >= 4 }
        return false
    }
    
    var isPublicHoliday: Bool {
        let month = Calendar.current.component(.month, from: self)
        let dayOfMonth = Calendar.current.component(.day, from: self)
        
        let holiday = Holiday(day: dayOfMonth, month: month)
        if (allHolidays.firstIndex(where: {$0 == holiday}) != nil) {
            return true
        }
        return false
    }
    
    func toString(_ format: String = "MM/dd/yyyy", locale: Locale = Locale(identifier: "en_US_POSIX")) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

// Christmas
var allHolidays: [Holiday] = [
    Holiday(day: 1, month: 1),
    Holiday(day: 10, month: 4),
    Holiday(day: 25, month: 5),
    Holiday(day: 15, month: 6),
    Holiday(day: 30, month: 7),
    Holiday(day: 31, month: 7),
    Holiday(day: 7, month: 9),
    Holiday(day: 11, month: 11),
    Holiday(day: 25, month: 12),
    Holiday(day: 28, month: 12), 
   // Holiday(day: 27, month: 2), // today, just to test
]
struct Holiday {
    var day: Int
    var month: Int
    
    static func ==(lhs: Holiday, rhs: Holiday) -> Bool {
        return lhs.day == rhs.day && lhs.month == rhs.month
    }
}
