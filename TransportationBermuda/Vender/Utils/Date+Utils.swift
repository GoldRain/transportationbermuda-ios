import Foundation

func getMessageTimeFromTimeStamp(_ timeStamp: Int64) -> String {
    
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
    let dateFormatter = DateFormatter()
    //dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
    dateFormatter.locale = NSLocale.current
    dateFormatter.dateFormat = "HH:mm, dd MMM, YYYY" //Specify your format that you want
    let strDate = dateFormatter.string(from: date)
    return strDate
}

func getCurrentTimeStamp() -> Int64 {
    return Int64(NSDate().timeIntervalSince1970)
}

func getStrDateTime(_ tstamp: String) -> String {
        
        let date = Date(timeIntervalSince1970: TimeInterval(tstamp)!/1000)
        
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd HH:mm" //Specify your format that you want MM-dd HH:mm format okay
        
        let strDate = dateFormatter.string(from: date)
        
        return strDate
}


func toMillis(_ dateVal: Date) -> Int64! {
    return Int64(dateVal.timeIntervalSince1970 * 1000)
}

let tstamp = toMillis(Date())
