////
////  favorite2.swift
////  Ferry Schedule
////
////  Created by Maude Chiasson on 2020-01-02.
////  Copyright © 2020 Maude Chiasson. All rights reserved.
//// the tableview is here and the Isee
//
import UIKit
import Foundation
import Firebase

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var viwFerryType: UIView!
    @IBOutlet weak var stationslabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    
}


struct FavoriteItem {
    var from: RouteSchedule
    var to: StationSchedule
    var nextHour: String?
    var hoursString = ""
    init(from: RouteSchedule, to: StationSchedule) {
        self.from = from
        self.to = to
        var hours = [String]()
        for day in to.days {
            if day.title == Date().dayOfWeek() {
                hours = day.times
                break
            }
        }
        
        
        hoursString = hours.joined(separator: "    ")
        if let nextHourIndex = DaySchedule.getNextTimeIndex(times: hours) {
            nextHour = hours[nextHourIndex]
        }
    }
}

class FavoriteDetailViewController: BaseVC, UIPickerViewDelegate, UIPickerViewDataSource {

//    var schedule = FerrySchedule.schedule
    var schedule: FerrySchedule!
    var route: RouteSchedule?
    var days: [DaySchedule] = []
    var doc_id : [String] = []
    var station_id : [Int] = []
    var datasource = [FavoriteItem]()
    var nextTimeIndex: Int?
    
    @IBOutlet weak var routeTextField: UITextField!
    @IBOutlet weak var satationTextField: UITextField!
    
    @IBOutlet weak var image: UIImageView!
    
    //@IBOutlet weak var routesPickerview: UIPickerView!
//    @IBOutlet weak var stationsPickerView: UIPickerView!
    @IBOutlet weak var Tableview: UITableView!
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarwidthconstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarwidthconstraintpickerview1: NSLayoutConstraint!
    @IBOutlet weak var avatarwidhtconstraintpickerview2: NSLayoutConstraint!
    
    var selectedRouteIndex = -1
    var selectedStationIndex = -1
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.schedule = FerrySchedule.schedule
        
//        let url = Bundle.main.url(forResource: "SectionData", withExtension: "json")
//        let data = try! Data(contentsOf: url!)
//        self.schedule = try! JSONDecoder().decode(FerrySchedule.self, from: data)
        self.schedule.connect()
        
//        if view.frame.height <= 736 {
//            avatarheightconstraint.constant = 50
//        } else {
//            avatarheightconstraint.constant = 75
//        }
//        image.needsUpdateConstraints()
//
//        if view.frame.height <= 736 {
//            avatarwidthconstraint.constant = 50
//        } else {
//            avatarwidthconstraint.constant = 75
//        }
//        image.needsUpdateConstraints()
        
//        if view.frame.height <= 736 {
//            avatarwidthconstraintpickerview1.constant = 200
//        } else {
//            avatarwidthconstraintpickerview1.constant = 250
//        }
//        routesPickerview.needsUpdateConstraints()
//       
//        if view.frame.height <= 736 {
//            avatarwidhtconstraintpickerview2.constant = 200
//        } else {
//            avatarwidhtconstraintpickerview2.constant = 250
//        }
//        stationsPickerView.needsUpdateConstraints()
        Tableview.dataSource = self
        Tableview.delegate = self
        Tableview.tableFooterView = UIView()
        
        getFavData()
        
        initPickerViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //show NotificationBox
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    

    func initPickerViews() {

        routeTextField.placeholder = "Route"
        satationTextField.placeholder = "Satation"
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        //routes
        let picker: UIPickerView
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 300))
        picker.backgroundColor = .white
        picker.tag = 0
        picker.delegate = self
        picker.dataSource = self

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker(_:)))
        doneButton.tag = 0
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        routeTextField.inputView = picker
        routeTextField.inputView?.height = 200
        routeTextField.inputAccessoryView = toolBar
        
        //routes
        let pickerSatation = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 300))
        pickerSatation.backgroundColor = .white
        pickerSatation.delegate = self
        pickerSatation.dataSource = self
        pickerSatation.tag = 1
        
        let done2Button = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker(_:)))
        done2Button.tag = 1
        
        let toolBarSatation = UIToolbar()
        toolBarSatation.barStyle = UIBarStyle.default
        toolBarSatation.isTranslucent = true
        toolBarSatation.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBarSatation.sizeToFit()
        toolBarSatation.setItems([spaceButton, done2Button], animated: false)
        toolBarSatation.isUserInteractionEnabled = true
        satationTextField.inputView = pickerSatation
        satationTextField.inputView?.height = 200
        satationTextField.inputAccessoryView = toolBarSatation
        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return self.schedule.routes.count
        } else {
            return self.route?.stations.count ?? 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return schedule.routes[row].title
        } else {
            return route?.stations[row].title
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            
            selectedRouteIndex = row
            selectedStationIndex = 0
            
            routeTextField.text = schedule.routes[row].title
            self.route = self.schedule.routes[row]
            satationTextField.text = route?.stations[0].title
//            self.stationsPickerView.reloadAllComponents()
        } else {
            selectedStationIndex = row
            satationTextField.text = self.route?.stations[row].title
        }
    }

    @objc func donePicker(_ sender: UIBarButtonItem) {
        
        if sender.tag == 0 {
            routeTextField.resignFirstResponder()
        } else {
            satationTextField.resignFirstResponder()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func getFavData(){
        Firestore.firestore().collection("users").document("\(User.currentUser.id!)").collection("preferences").getDocuments(){(querySnapshot, err) in
            if err != nil {
                print("error")
            }else{
                for document in (querySnapshot?.documents)!{
                    let data = document.data()
                    self.doc_id.append(document.documentID)
                    let fromIndex = data["route"] as? Int
                    let fromStation = self.schedule.routes[fromIndex!]
                    
                    let toIndex = data["station"] as? Int
                    self.station_id.append(toIndex!)
                    let toStation = self.schedule.routes[fromIndex!].stations[toIndex!]
                    
                    let item = FavoriteItem(from: fromStation, to: toStation)
                    self.datasource.append(item)
                }
                self.Tableview.reloadData()
            }
        }
    }
    

    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if pickerView == self.routesPickerview {
//            self.route = self.schedule.routes[row]
//            self.stationsPickerView.reloadAllComponents()
//        }
//    }
    
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//        var pickerLabel: UILabel? = (view as? UILabel)
//        if pickerLabel == nil {
//            pickerLabel = UILabel()
//            pickerLabel?.font = UIFont.systemFont(ofSize: 16.0)
////            pickerLabel?.font = UIFont.init(name: "Didot", size: 20)
//            pickerLabel?.textAlignment = .center
//        }
//
//        var routerTitle: String!
//        if pickerView == self.routesPickerview {
//            routerTitle = self.schedule.routes[row].title
//        } else {
//            routerTitle = self.route?.stations[row].title
//        }
//        pickerLabel?.text = routerTitle
//
//        return pickerLabel!
//    }
    
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//        if pickerView == self.routesPickerview {
//            return self.schedule.routes.count
//        } else {
//            return self.route?.stations.count ?? 0
//        }
//    }
    
    private func removeSignUpFlow() {
        var viewcontrollers = self.navigationController!.viewControllers
        for (index, viewController) in viewcontrollers.enumerated() {
            if viewController is Favorite {
                viewcontrollers.remove(at: index)
            } else if viewController is SignUpEmail {
                viewcontrollers.remove(at: index)
            }
        }
        self.navigationController!.viewControllers = viewcontrollers
    }
    
    @IBAction func favButtonTapped (){
        
        //let fromIndex = routesPickerview.selectedRow(inComponent: 0)
        //let fromStation = schedule.routes[fromIndex]
        
        let fromStation = schedule.routes[selectedRouteIndex]
        
        //let toIndex = stationsPickerView.selectedRow(inComponent: 0)
        //let toStation = schedule.routes[fromIndex].stations[toIndex]

        let toStation = schedule.routes[selectedRouteIndex].stations[selectedStationIndex]
        
        let item = FavoriteItem(from: fromStation, to: toStation)
        datasource.append(item)

        Tableview.reloadData()
    }
    
    func callDeleteItem(_ indexPath : IndexPath) {
        
        showAlert(title: nil, message: "Do you want to delete this item?", positive: "Delete", negative: "Cancel") {
            self.showLoadingView(vc: self)
           
            let index = indexPath.row
            Firestore.firestore().collection("users").document("\(User.currentUser.id!)").collection("preferences").document(self.doc_id[index]).delete(){err in
                self.hideLoadingView()
                if err != nil{
                    self.showError(err?.localizedDescription)
                }else{
                    self.datasource.remove(at: index)
                    self.station_id.remove(at: index)
                    self.doc_id.remove(at: index)
    //                self.Tableview.reloadData()
                    self.Tableview.deleteRows(at: [indexPath], with: .bottom)
                }
            }
        }
    }

    @IBAction func favButtonTap() {
        
        if (routeTextField.text!.isEmpty){
            showError("Please select route")
            return
        }
        
            favButtonTapped()

//        let selectedRoute = routesPickerview.selectedRow(inComponent: 0)
//        let selectedStation = stationsPickerView.selectedRow(inComponent: 0)
        
        NetworkServices.saveUserPreference(route: selectedRouteIndex, station: selectedStationIndex) { (isAdded,id) in
            if isAdded == true {
                self.station_id.append(self.selectedStationIndex)
                self.doc_id.append(id!)
                print("Data daved!")
            }
        }
    }
}

extension FavoriteDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell", for: indexPath) as! FavoriteTableViewCell
        let item = datasource[indexPath.row]
        
        cell.stationslabel?.text = item.to.title
        cell.stationslabel?.textColor = item.from.headerColor
        //cell.timelabel?.text = hours.joined(separator: "   ")
        let hoursString = item.hoursString
        if let nextHour = item.nextHour {
            let attString = String.format(strings: [nextHour],
                                          boldFont: UIFont.boldSystemFont(ofSize: 14),
                                          boldColor: /*.cyan*/item.from.headerColor,
                                          inString: hoursString,
                                          font: cell.timelabel.font,
                                          color: .black)
            cell.timelabel.attributedText = attString
        } else {
            cell.timelabel?.text = hoursString
            cell.timelabel.textColor = .black
        }
        
        //cell.viwFerryType.backgroundColor = item.from.headerColor
        
        if indexPath.section == 0, indexPath.row == nextTimeIndex {
            //cell.textLabel?.textColor = UIColor.cyan
            cell.textLabel?.textColor = item.from.headerColor
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index_id = station_id[indexPath.row]
        let station = datasource[indexPath.row].from.stations[index_id]
        let station_image = datasource[indexPath.row].from.headerImageName
        let station_header = datasource[indexPath.row].from.headerColor
        
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteScheduleViewController") as! FavoriteScheduleViewController
        controller.schedule_image = station_image
        controller.schedule_header = station_header
        controller.datasource = datasource
        controller.schedule = station
        self.navigationController?.pushViewController(controller, animated: true)
        
    }//check
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let title = NSLocalizedString("Delete", comment: "delete")

        let action = UIContextualAction(style: .normal, title: title, handler: { (action, view, completionHandler) in
            self.callDeleteItem(indexPath)
            completionHandler(true)
        })

//        action.image = UIImage(named: "ic_waste")
        action.backgroundColor = CONSTANT.COLOR_PRIMARY
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}

extension String {
    static func format(strings: [String],
                       boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont.systemFont(ofSize: 14),
                       color: UIColor = UIColor.black,
                       lineSpacing: CGFloat = 7,
                       alignment: NSTextAlignment = .left) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.maximumLineHeight = 40
        paragraphStyle.alignment = alignment
        
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color,
                                        NSAttributedString.Key.paragraphStyle: paragraphStyle])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont,
                                 NSAttributedString.Key.foregroundColor: boldColor]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
}
