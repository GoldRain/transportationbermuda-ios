//
//  FavoriteTVC.swift
//  TransportationBermuda
//
//  Created by Ahmad on 2021/3/9.
//  Copyright © 2021 Maude Chiasson. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FBSDKLoginKit
import SwiftyJSON

class FavoriteTVC: UITableViewController, GIDSignInDelegate {

    var schedule: FerrySchedule!
    var fbLoginManager : LoginManager? = nil

 
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var appleButton: UIStackView!
   
    //@IBOutlet weak var stackview: UIStackView!
    //@IBOutlet weak var avatarheightcontraintstackview: NSLayoutConstraint!
    
//    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var email = ""
    var password = ""
    
    override func viewDidLoad() {
        
        self.schedule = FerrySchedule.schedule
        
        let url = Bundle.main.url(forResource: "SectionData", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        self.schedule = try! JSONDecoder().decode(FerrySchedule.self, from: data)
        self.schedule.connect()
        fbLoginManager = LoginManager()
        
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
      
        image.needsUpdateConstraints()
        
//        if
//            view.frame.height <= 736 {
//            avatarheightcontraintstackview.constant = 0
//        } else {
//            avatarheightcontraintstackview.constant = 100
//        }
//        stackview.needsUpdateConstraints()

        
        //google signin
//        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //addButtonNavBar()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let loginStatus = UserDefaults.standard.bool(forKey: "loginStatus")
        
        if loginStatus{
            userLogin()
        }
    }
    
    //MARK:- Private Methods
    func saveUserInformation() {
        var userDic = [String: Any]()
        userDic["id"]       = Auth.auth().currentUser!.uid
        userDic["email"]    = Auth.auth().currentUser!.email
        User.currentUser.saveUserInfo(userInfo: userDic)
    }
    
    @IBAction func onClickFacebook(_ sender: Any) {
        fbLoginManager = LoginManager()
        fbLoginManager?.logOut()
        
        showLoadingView(vc: self, label: R.string.REPORT_LOADING)
        fbLoginManager!.logIn(permissions: ["public_profile", "email"/*, "user_about_me"*/], from: self) { (result, error) in
            if (error == nil){

                guard let accessToken = AccessToken.current else {
                    print("Failed to get access token")
                    self.hideLoadingView()
                    return
                }
                
                
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)

                // Perform login by calling Firebase APIs
                Auth.auth().signIn(with: credential, completion: {(authResult, error) in
                    self.hideLoadingView()
                    if let error = error {
                        self.fbLoginManager?.logOut()
                        print("Login error: \(error.localizedDescription)")
//                        self.showAlert(title: R.string.APP_NAME, message: error.localizedDescription)
                        self.showError(error.localizedDescription)
                        return
                    }
                    
                    //save User data
                    self.saveUserInformation()
                    
                    self.fbLoginManager?.logOut()
                    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
                    self.navigationController?.pushViewController(controller, animated: true)
                })
            } else {
                self.hideLoadingView()
                print("Login failed")
            }
        }
    }
    
//    func getAndSaveFBUserData(_ user: Firebase.User){
//        if((AccessToken.current) != nil) {
//            GraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, first_name, last_name"]).start(completionHandler: {
//                (connection, result, error) -> Void in
//
//                if (error == nil) {
//
//                    let jsonResponse = JSON(result!)
//
//                    print("jsonResponse", jsonResponse)
//                    let id = jsonResponse["id"].stringValue
//
//                    let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
//                    let first_name = jsonResponse["first_name"].string ?? "unknown"
//                    let last_name = jsonResponse["last_name"].string ?? "unknown"
//                    let photoUrl = "https://graph.facebook.com/" + id + "/picture?type=large"
                    
//                    let data = [
//                        kFIRST_NAME     : first_name,
//                        kLAST_NAME      : last_name,
//                        kPHONENUMBER    : "",
//                        kEMAIL          : email,
//                        kID_NUMBER      : "",
//                        kPHOTO_URL      : photoUrl,
//                        kFCM_TOKEN      : deviceTokenString,
//                        kCONTACT_ID     : [],
//                        kRECEIVED_ID    : [],
//                        "visa":"0",
//                        "fechavisa":"2020-08-01",
//                        "flagnoti":"0"
//                    ] as [String : Any]
                    
//                    self.fbLoginManager?.logOut()
//                    self.saveUserInfo(user, data)
//                } else {
//                    // TODO:  Exeption  ****
//                    print(error!)
//                }
//            })
//        } else {
//            print("token is null")
//        }
//    }
    
    @IBAction func onclicksignin(_ sender: Any) {
        
        email = emailTextField.text!
        password = passwordTextField.text!
        
        if email.isEmpty {
            showAlert("Enter email")
            return
        }
        if password.isEmpty {
            showAlert("Enter password")
            return
        }
        
        //Firebase Auth login
        
        self.showLoadingView()
        
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            if let e = error {
                print(e.localizedDescription)
                self.showAlert(e.localizedDescription)
            } else {
                
                self.hideLoadingView()
                
                self.saveUserInformation()
                self.userLogin()
            }
        }
    }
    
    func userLogin(){
        
        hideLoadingView()
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //google signin button
    @IBAction func googleButtonTap(sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
}

//MARK:- google signin delegate methods
extension Favorite: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            print("Error - Google Signin")
            print(error)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        showLoadingView(vc: self, label: R.string.REPORT_LOADING)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            self.hideLoadingView()
            
            if let error = error {
                print("Error - Google Signin")
                print(error)
                return
            }
            self.saveUserInformation()
            print("Google SignIn Complete!")
            //self.performSegue(withIdentifier: "SignUpToFav2", sender: self)
            
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
