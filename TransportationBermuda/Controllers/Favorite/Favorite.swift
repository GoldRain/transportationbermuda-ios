//
//  Favorite.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-21.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//
//
import UIKit
import GoogleSignIn
import Firebase
import FBSDKLoginKit
import SwiftyJSON
import AuthenticationServices
import CryptoKit


class Favorite: BaseVC {
    
    var schedule: FerrySchedule!
    var fbLoginManager : LoginManager? = nil

 
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var signWithAppleStack: UIStackView!
   
    //@IBOutlet weak var stackview: UIStackView!
    //@IBOutlet weak var avatarheightcontraintstackview: NSLayoutConstraint!
    
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    var email = ""
    var password = ""
    // Unhashed nonce.
    fileprivate var currentNonce: String?
    
    
    override func viewDidLoad() {
       
        self.schedule = FerrySchedule.schedule
        
        let url = Bundle.main.url(forResource: "SectionData", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        self.schedule = try! JSONDecoder().decode(FerrySchedule.self, from: data)
        self.schedule.connect()
        fbLoginManager = LoginManager()
        setupSignInAppleButton()
        
        super.viewDidLoad()
        
        if
            view.frame.height <= 736 {
            avatarheightconstraint.constant = 225
        } else {
            avatarheightconstraint.constant = 250
        }
        image.needsUpdateConstraints()
        
//        if
//            view.frame.height <= 736 {
//            avatarheightcontraintstackview.constant = 0
//        } else {
//            avatarheightcontraintstackview.constant = 100
//        }
//        stackview.needsUpdateConstraints()

        
        //google signin
//        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let loginStatus = UserDefaults.standard.bool(forKey: "loginStatus")
        
        if loginStatus{
            userLogin()
        }
    }
    
    //MARK:- Private Methods
    func saveUserInformation() {
        var userDic = [String: Any]()
        userDic["id"]       = Auth.auth().currentUser!.uid
        userDic["email"]    = Auth.auth().currentUser!.email
        User.currentUser.saveUserInfo(userInfo: userDic)
    }
    
    //apple button config
    func setupSignInAppleButton() {
        let authorizationButton = ASAuthorizationAppleIDButton()
        authorizationButton.addTarget(self, action: #selector(appleIDButtonPressed), for: .touchUpInside)
        signWithAppleStack.addArrangedSubview(authorizationButton)
    }
    
    //MARK: - perform appleid request
    @objc
    func appleIDButtonPressed() {
        
        let nonce = randomNonceString()
        currentNonce = nonce
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    // make random string with charset
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> = Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length

        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }

            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }

                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }

        return result
    }
    
    // make sha256 key
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()

        return hashString
    }
    
    //MARK:- action facebook button
    @IBAction func onClickFacebook(_ sender: Any) {
        fbLoginManager = LoginManager()
        fbLoginManager?.logOut()
        
        showLoadingView(vc: self, label: R.string.REPORT_LOADING)
        fbLoginManager!.logIn(permissions: ["public_profile", "email"/*, "user_about_me"*/], from: self) { (result, error) in
            if (error == nil){

                guard let accessToken = AccessToken.current else {
                    print("Failed to get access token")
                    self.hideLoadingView()
                    return
                }
                
                
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)

                // Perform login by calling Firebase APIs
                Auth.auth().signIn(with: credential, completion: {(authResult, error) in
                    self.hideLoadingView()
                    if let error = error {
                        self.fbLoginManager?.logOut()
                        print("Login error: \(error.localizedDescription)")
//                        self.showAlert(title: R.string.APP_NAME, message: error.localizedDescription)
                        self.showError(error.localizedDescription)
                        return
                    }
                    
                    self.saveUserAndGotoFavorite2()
                })
            } else {
                self.hideLoadingView()
                print("Login failed")
            }
        }
    }
    
    func saveUserAndGotoFavorite2() {
        //save User data
        self.saveUserInformation()
        
        self.fbLoginManager?.logOut()
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onclicksignin(_ sender: Any) {
        
        email = emailTextField.text!
        password = passwordTextField.text!
        
        if email.isEmpty {
            showAlert("Enter email")
            return
        }
        if password.isEmpty {
            showAlert("Enter password")
            return
        }
        
        //Firebase Auth login
        
        self.showLoadingView()
        
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            if let e = error {
                print(e.localizedDescription)
                self.showAlert(e.localizedDescription)
            } else {
                
                self.hideLoadingView()
                
                self.saveUserInformation()
                self.userLogin()
            }
        }
    }
    
    func userLogin(){
        
        hideLoadingView()
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //google signin button
    @IBAction func googleButtonTap(sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
}

//MARK:- apple signin delegate
extension Favorite: ASAuthorizationControllerDelegate {

    //on success
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        showLoadingView(vc: self, label: R.string.REPORT_LOADING)
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nonce)
            // Sign in with Firebase.
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if error != nil {
                    // Error. If error.code == .MissingOrInvalidNonce, make sure
                    // you're sending the SHA256-hashed nonce as a hex string with
                    // your request to Apple.
                    print(error!.localizedDescription)
                    return
                }
                // User is signed in to Firebase with Apple.
                self.hideLoadingView()
                self.saveUserAndGotoFavorite2()
            }
        }
    }

    //on error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
    }

}

//MARK:- apple signin page presentation
extension Favorite: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

//MARK:- google signin delegate methods
extension Favorite: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            print("Error - Google Signin")
            print(error)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        showLoadingView(vc: self, label: R.string.REPORT_LOADING)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            self.hideLoadingView()
            
            if let error = error {
                print("Error - Google Signin")
                print(error)
                return
            }
            self.saveUserInformation()
            print("Google SignIn Complete!")
            //self.performSegue(withIdentifier: "SignUpToFav2", sender: self)
            
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
    
}
