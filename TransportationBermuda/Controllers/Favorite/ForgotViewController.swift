//
//  ForgotViewController.swift
//  TransportationBermuda
//
//  Created by Ahmad on 2021/2/21.
//  Copyright © 2021 Maude Chiasson. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotViewController: BaseVC {

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onClickSend(_ sender: Any) {
        
        let email = emailTextField.text!
        
        if email.isEmpty {
            showAlert("Enter email")
            return
        }
        else {
            showLoadingView(vc: self)
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                self.hideLoadingView()
                if error != nil {
                    self.showError((error?.localizedDescription)!)
                } else {
                    //email = ""
                    self.showAlert("Password reset email sent.")
                }
            })
        }
        
        
    
    }
    
    @IBAction func onClickLoginPage(_ sender: Any) {
        
        doDismiss()
    }
}
