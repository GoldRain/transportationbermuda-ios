//
//  SignUpEmail.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-12.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//
//
import UIKit
import Firebase

class SignUpEmail: BaseVC {
    //MARK:- Interface Builder
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    //MARK:- Viewcontroller LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if
//            view.frame.height <= 568 {
//            avatarheightconstraint.constant = 150
//        } else {
//            avatarheightconstraint.constant = 200
//        }
//        image.needsUpdateConstraints()
    }
    
    //MARK:- Private Methods
    private func isValidateInputs() -> Bool {
        if emailTextField.text! == "" {
            return false
        } else if passwordTextField.text! == "" {
            return false
        } else if confirmPasswordTextField.text! == "" {
            return false
        } else if passwordTextField.text! != confirmPasswordTextField.text! {
            return false
        } else {
             return true
        }
    }
    
    
}

//MARK:- Button Actions
extension SignUpEmail {
    @IBAction func signUpButtonTap(sender: UIButton) {
        let email = self.emailTextField.text!
        let password = self.passwordTextField.text!
        
        self.showLoadingView()
        if isValidateInputs() {
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if error != nil {
                    
                    self.hideLoadingView()
                    
                    print("Error - Signup using email!")
                    print(error!)
                    self.showAlert(error?.localizedDescription)
                    
                } else {
                    
                    self.hideLoadingView()
                    
                    var userDic = [String: Any]()
                    userDic["id"] = Auth.auth().currentUser!.uid
                    userDic["email"] = Auth.auth().currentUser!.email
                    User.currentUser.saveUserInfo(userInfo: userDic)
                    print("Signup Success!")
                    
                    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favorite2") as! FavoriteDetailViewController
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            //self.performSegue(withIdentifier: "SignUpEmailToFav2", sender: self)
        } else {
            self.hideLoadingView()
//            self.showAlert(title: "Input Error", message: "Please enter valid inputs.")
            self.showError( "Please enter valid inputs.")
            
            print("Inputs are not valid!")
        }
        //self.performSegue(withIdentifier: "SignUpEmailToFav2", sender: self)
    }
}
