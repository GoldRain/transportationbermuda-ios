//
//  FavoriteDetailScheduleViewController.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-25.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
////
//

import UIKit

class RouteTimeTableViewCell2: UITableViewCell {
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var TimeLabel: UILabel!
}

class FavoriteDetailScheduleViewController: BaseVC, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {
    
    var schedule: RouteTimes! { didSet { self.updateSchedule() }}
    var days: [DaySchedule] = []
    var nextTimeIndex: Int?
    var schedule_image : String = ""
    var schedule_header = UIColor()
    var schedule_day : String = ""
    var schedule_title : String = ""
    var datasource = [FavoriteItem]()
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarheightconstraintimage: NSLayoutConstraint!
    @IBOutlet weak var titleUIview: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    func updateSchedule() {
        if let stops = schedule?.stops {
            let times = stops.map({ return $0.time })
            nextTimeIndex = DaySchedule.getNextTimeIndex(times: times)
        }
        
        self.tableView?.reloadData()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schedule?.stops.count ?? 0
        
    }
    /*
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    /*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  header = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 25))
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: header.bounds.width, height: 25))
        let label2 = UILabel(frame: CGRect(x: -10, y: 0, width: header.bounds.width, height: 25))
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label2.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //label.font = UIFont (name: "Didot-Bold", size: 22)
        label.font = UIFont (name: "Copperplate-Bold", size: 22)
        //label2.font = UIFont (name: "Didot", size: 18)
        label2.font = UIFont (name: "Copperplate", size: 18)
        label.textColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 1.0)
        
//        label.text = self.datasource[section].from.stations[section].title
//        label2.text = self.datasource[section].from.stations[section].days[section].title
        
                
        label.text = self.schedule_title
        label2.text = self.schedule_day
                    
        label2.textAlignment = .right
        header.addSubview(label)
        header.addSubview(label2)
        header.backgroundColor =  schedule_header
        
        return header
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RouteTimeTableViewCell", for: indexPath) as! RouteTimeTableViewCell
        //cell.NameLabel?.font = UIFont (name: "Didot", size: 18)
        cell.NameLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        cell.TimeLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        
        cell.NameLabel?.text = self.schedule.stops[indexPath.row].stop
        cell.TimeLabel?.text = self.schedule.stops[indexPath.row].time
      
        if indexPath.row == nextTimeIndex {
            //cell.TimeLabel?.textColor = UIColor.cyan
            //cell.TimeLabel?.textColor = schedule_header
            cell.backgroundColor = schedule_header
        } else {
            cell.backgroundColor = .white
        }
        return cell
    }
    
    override func viewDidLoad() {
        
        //set image
        self.image.image = UIImage(named: self.schedule_image)

        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if view.frame.height <= 736 {
            avatarheightconstraint.constant = 175
        } else {
            avatarheightconstraint.constant = 225
        }
        scrollview.needsUpdateConstraints()
        
        if view.frame.height <= 736 {
            avatarheightconstraintimage.constant = 175
        } else {
            avatarheightconstraintimage.constant = 225
        }
        image.needsUpdateConstraints()
        
        //titleLabel.text = self.schedule_title
        titleLabel.text = self.schedule?.daySchedule?.stationSchedule?.route?.title
        //dateLabel.text = self.schedule_day
        titleUIview.backgroundColor = schedule_header
        
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    
    
}



