//
//  FourroutesViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-05.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//

import UIKit

class FerryRoutesViewController: BaseVC {
    
    var schedule = FerrySchedule.schedule
    
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var stackview: UIStackView!

    // The Interface Builder Outlets
//     @IBOutlet weak var OrangeRoute
     @IBOutlet weak var myLabel: UILabel!
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.schedule.connect()
        
//        if view.frame.height <= 736 {
//            avatarheightconstraint.constant = 80
//        } else {
//            avatarheightconstraint.constant = 100
//        }
//        stackview.needsUpdateConstraints()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
  
    
    @IBAction func showStationsViewController(_ sender: UIButton) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FerryStationsViewController") as! FerryStationsViewController
        
        controller.route = self.schedule.routes[sender.tag]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    
    @IBAction func showAlertButtonTapped(_ sender: UIButton) {
        
        // create the alert
        let alert = UIAlertController(title: "The orange route is not running during the Winter Season", message: "", preferredStyle: UIAlertController.Style.alert )
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

