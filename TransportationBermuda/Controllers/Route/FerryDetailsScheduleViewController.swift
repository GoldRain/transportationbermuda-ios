//
//  FerryDetails.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-12.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit

class RouteTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var TimeLabel: UILabel!
}


class FerryDetailsScheduleViewController: BaseVC, UINavigationControllerDelegate {
    
    var schedule: RouteTimes! { didSet { self.updateSchedule() }}
    var schedule_title = ""
    
    var nextTimeIndex: Int?
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var route: UILabel!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var avatarheightconstraintimage: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        self.updateHeaderImage()
        super.viewDidLoad()
              
        route.frame = CGRect(x: 0, y: 5, width: 40, height: 25)
        route.text = self.schedule?.daySchedule?.stationSchedule?.route?.title
        //route.text = schedule_title
        route.textAlignment = .center
        route.font = UIFont (name: "Copperplate", size: 22)
        header.backgroundColor = self.schedule?.daySchedule?.stationSchedule?.route?.headerColor

        if view.frame.height <= 736 {
            avatarheightconstraint.constant = 175
            avatarheightconstraintimage.constant = 175
        } else {
            avatarheightconstraint.constant = 250
            avatarheightconstraintimage.constant = 250
        }
        scrollview.needsUpdateConstraints()

        image.needsUpdateConstraints()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    
    //MARK:- custom functions
    func updateSchedule() {
        if let stops = schedule?.stops {
            let times = stops.map({ return $0.time })
            nextTimeIndex = DaySchedule.getNextTimeIndex(times: times)
        }
        self.tableView?.reloadData()
    }
    
    func updateHeaderImage() {
        if let name = self.schedule?.daySchedule?.stationSchedule?.route?.headerImageName {
            self.image.image = UIImage(named: name)
        }
    }
    
}

extension FerryDetailsScheduleViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schedule?.stops.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RouteTimeTableViewCell", for: indexPath) as! RouteTimeTableViewCell
        cell.NameLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        cell.TimeLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        cell.NameLabel?.text = self.schedule.stops[indexPath.row].stop
        cell.TimeLabel?.text = self.schedule.stops[indexPath.row].time
        
        if indexPath.row == nextTimeIndex {
            cell.TimeLabel?.font  = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
            cell.NameLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
            cell.backgroundColor = schedule.daySchedule?.stationSchedule?.route?.headerColor
        } else {
            cell.backgroundColor = .white
        }
        return cell
    }
    
}
