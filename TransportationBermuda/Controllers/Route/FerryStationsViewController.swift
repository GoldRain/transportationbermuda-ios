//
//  RoutesViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-09.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//


import UIKit

class FerryStationsViewController: BaseVC, UINavigationControllerDelegate {
    
    var route: RouteSchedule!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollview: UIScrollView!

    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarheightconstraintimage: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set image
        self.image.image = UIImage(named: self.route.headerImageName)
        if view.frame.height <= 736 {
            avatarheightconstraint.constant = 175
            avatarheightconstraintimage.constant = 175
        } else {
            avatarheightconstraint.constant = 250
            avatarheightconstraintimage.constant = 250
        }
        scrollview.needsUpdateConstraints()
        image.needsUpdateConstraints()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }

}

extension FerryStationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.route.stations.count
    }

   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let header = UIView(frame: CGRect(x: 0, y: 5, width: 40, height: 25))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: header.bounds.width, height: header.bounds.height))
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.font = UIFont (name: "Copperplate", size: 22)
        label.textColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 1.0)
        label.text = route.title
        label.textAlignment = .center
        header.addSubview(label)
        header.backgroundColor = route.headerColor
    
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        cell.textLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        cell.textLabel?.text = self.route.stations[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let station = self.route.stations[indexPath.row]
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FerryScheduleViewController") as! FerryScheduleViewController
        controller.schedule = station
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
