//
//  FerryScheduleViewController.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-21.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit


class DayScheduleVM {
    var data: DaySchedule
    var isOn: Bool = false
    
    init(data: DaySchedule, isOn: Bool = false) {
        self.data = data
        self.isOn = isOn
    }
}


class FerryScheduleViewController: BaseVC, UINavigationControllerDelegate {

    var schedule: StationSchedule! { didSet { self.updateSchedule() }}
    var days: [DayScheduleVM] = []
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var route: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarheightconstraintimage: NSLayoutConstraint!
    var nextTimeIndex: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set image
        self.image.image = UIImage(named: (self.schedule.route?.headerImageName)!)
        
        //set label
        route.frame = CGRect(x: 0, y: 5, width: 40, height: 25)
        route.text = schedule.title
        route.font = UIFont (name: "Copperplate", size: 22)
        route.textAlignment = .center
        route.backgroundColor = schedule.route?.headerColor
        
        view.addSubview(route)

        if view.frame.height <= 736 {
            avatarheightconstraint.constant = 175
            avatarheightconstraintimage.constant = 175
        } else {
            avatarheightconstraint.constant = 225
            avatarheightconstraintimage.constant = 225
        }
        scrollview.needsUpdateConstraints()

        image.needsUpdateConstraints()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    
    //MARK:- custom fuction
    func updateSchedule() {
        
        let day = Calendar.current.component(.weekday, from: Date())
        let today = Date()
        let sundaySchedule = schedule.days[0]
        let holidaySources = [sundaySchedule] + Array(schedule.days[day..<7]) + Array(schedule.days[0..<( day - 1)])
        let sources = Array(schedule.days[(day - 1)..<7]) + Array(schedule.days[0..<( day - 1)])
        
        for item in (today.isPublicHoliday ? holidaySources : sources) {
            let vm = DayScheduleVM(data: item)
            days.append(vm)
        }
        
        days[0].isOn = true
        nextTimeIndex = DaySchedule.getNextTimeIndex(times: days[0].data.times)
        self.tableView?.reloadData()
    }
 
    @objc func toggleContent(button: UIButton) {
        let index = button.tag
        days[index].isOn = !days[index].isOn
        tableView.reloadData()
    }
    
}

extension FerryScheduleViewController :  UITableViewDataSource, UITableViewDelegate {
    
    //MARK:- tableview
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        cell.textLabel?.font = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
        cell.textLabel?.text = self.days[indexPath.section].data.times[indexPath.row]
        
        if indexPath.section == 0, indexPath.row == nextTimeIndex {
               cell.textLabel?.font  = UIFont (name: "AppleSDGothicNeo-Regular", size: 18)
               cell.backgroundColor = schedule.route?.headerColor

        } else {
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let station = self.days[indexPath.section].data.routeTimes?[indexPath.row]
        let station_title = schedule.title
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FerryDetailsScheduleViewController") as! FerryDetailsScheduleViewController
        
        controller.schedule = station
        controller.schedule_title = station_title
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.days.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if days[section].isOn {
            return self.days[section].data.times.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d"

        var title = formatter.string(for: Calendar.current.date(byAdding: .day, value: section, to: Date()))
        
        if section == 0 {
            title = "Today"
        }

        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 22)
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: button.bounds.width, height: 22))
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.font = UIFont (name: "AppleSDGothicNeo-SemiBold", size: 20)
        label.text = title
        label.textAlignment = .left
        label.textColor = UIColor.white

        button.backgroundColor = UIColor.lightGray
        button.tag = section
        button.addTarget(self, action: #selector(toggleContent), for: .touchUpInside)
        button.addSubview(label)
        
        return button
    }
}

