//
//  StoriesTableViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-03.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//

//import UIKit
//
//
//// Structure routes stations
//
////determine if date = public holiday (http://www.bermuda-online.org/pubhols.htm)
//    /*
//    if holidays in  ("01-01","04-10","05-29","06-15","07-30","07-31","09-09","11-11","12-25","12-26"){
//    return public_holiday = 1
//    }
//    else {
//    return  public_holidays = 0
//        
//        
//    }
//*/
//class StoriesTable: UITableViewController {
//    var schedule: StationSchedule! { didSet { self.updateSchedule() }}
//    var days: [DaySchedule] = []
//    
//    func updateSchedule() {
//        let day = Calendar.current.component(.weekday, from: Date())
//        self.days = Array(schedule.days[(day - 1)..<7]) + Array(schedule.days[0..<( day - 1)])
//        self.tableView?.reloadData()
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//    }
//    
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return self.days.count
//    }
//    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.days[section].times.count
//    }
//
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "EEEE, MMM d"
//      
//        if section == 0 {
//            return "Today"
//        }
//        
//        return formatter.string(for: Calendar.current.date(byAdding: .day, value: section, to: Date()))
//    }
//    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        
//        let header = view as! UITableViewHeaderFooterView
//        header.textLabel?.font = UIFont(name: "TimesNewRomanPS", size: 18)
//        header.textLabel?.textColor = UIColor.black
//    }
//
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
//        cell.textLabel?.font = UIFont (name: "Times New Roman", size: 20)
//
//        cell.textLabel?.text = self.days[indexPath.section].times[indexPath.row]
//        return cell
//    }
//    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let station = self.days[indexPath.section].routeTimes?[indexPath.row]
//  
//        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FerryDetails") as! FerryDetails
//        controller.schedule = station
//    
//
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
//}
//        
//    
