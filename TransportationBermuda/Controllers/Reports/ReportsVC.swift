//
//  ReportsVC.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit

class ReportsVC: BaseVC {

    //MARK:- outlets
    @IBOutlet weak var uiTableview: UITableView!
    
    // data for notification tableview
    var dataSource  = [ReportModel]()
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.title = "Notification"
        uiTableview.dataSource = self
        uiTableview.delegate = self
        uiTableview.tableFooterView = UIView()
        
        loadData()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.post(name: Notification.Name("changedBadgeCount"), object: nil)
        
        
    }
    
    //MARK:- custom functions
    
    // get and add Listener of Reports(Notification) colllection
    func loadData() {
        
        self.showLoadingView(vc: self, label: R.string.REPORT_LOADING)

        FirebaseAPI.addListenerReports { (isSuccess, data, changeType) in
            
            self.hideLoadingView()
            
            if isSuccess {
                
                let temp =  data as! [ReportModel]
                for oneTemp in temp {
                    
                    if changeType == .added {
                        self.dataSource.append(oneTemp)
                    } else if changeType == .removed {
                        self.dataSource.remove(at: self.dataSource.firstIndex(where: { (oneData) -> Bool in
                            oneData.row_id == oneTemp.row_id
                        })!)
                        
                        if self.dataSource.count == 0 {
                            self.doDismiss()
                        }
                    } else if changeType == .modified {
                        
                        self.dataSource.remove(at: self.dataSource.firstIndex(where: { (oneData) -> Bool in
                            oneData.row_id == oneTemp.row_id
                        })!)
                        self.dataSource.append(oneTemp)
                    }
                }
                
                self.dataSource.sort { (one, two) -> Bool in
                    if one.report_timestamp > two.report_timestamp {
                        return true
                    }
                    return false
                }
                
                self.uiTableview.reloadData()
            }
        }
    }
    
    // push Report detail viewcontroller with selected detail data.
//    func gotoDetailVC(_ indexPath: IndexPath) {
//        
//        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "ReportDetailVC") as! ReportDetailVC
//        toVC.detailData = dataSource[indexPath.row]
//        self.navigationController?.pushViewController(toVC, animated: true)
//    }
}


extension ReportsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        cell.entity = dataSource[indexPath.row]
//        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    // when click accessory Button on table view
//    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
//        gotoDetailVC(indexPath)
//    }
    
    // when click accessory Button on table view
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        gotoDetailVC(indexPath)
//    }
    
}

class ReportCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    //ReportModel
    var entity: ReportModel! {
        didSet {
            lblTitle.text  = entity.report_title
            lblTitle.numberOfLines = 0
            lblContent.text = entity.report_content
            lblContent.numberOfLines = 0
            lblTime.text    = getMessageTimeFromTimeStamp(entity.report_timestamp)
        }
    }
    

}
