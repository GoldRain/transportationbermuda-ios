////
////  ReportDetailVC.swift
////  TransportationBermuda
////
////  Created by Rapunzel on 11/24/20.
////  Copyright © 2020 Maude Chiasson. All rights reserved.
////
//
//import UIKit
//
//class ReportDetailVC: BaseVC {
//
//    //MARK:- outlets
//    @IBOutlet weak var lblCTitle: UILabel!
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblCContent: UILabel!
//    @IBOutlet weak var lblContent: UILabel!
//    @IBOutlet weak var lblTime: UILabel!
//    
//    var detailData: ReportModel? = nil
//    
//    //MARK:- Life cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.title = ""
//        
//        //if detail data is null this viewcontroller will pop
//        // and detail data is exist, this will set detail data
//        if detailData != nil {
//            lblCTitle.underline()
//            lblCContent.underline()
//            
//            lblTitle.text               = detailData?.report_title
//            lblTitle.numberOfLines      = 0
//            lblContent.text             = detailData?.report_content
//            lblContent.numberOfLines    = 0
//            lblTime.text                = getMessageTimeFromTimeStamp(detailData!.report_timestamp)
//            lblTime.numberOfLines       = 0
//            
//        } else {
//            doDismiss(true)
//        }
//    }
//    
//}
