//
//  SummaryViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-06.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//

import UIKit

class Summary: BaseVC {

    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var avatarheightconstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if view.frame.height <= 736 {
            avatarheightconstraint.constant = 225
        } else {
            avatarheightconstraint.constant = 250
        }
        image.needsUpdateConstraints()

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }

    
    //MARK:- custom actions
    //open the 4 tables
    @IBAction override func showFerryRoutesController() {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "MainTab") as! UITabBarController
        vc.selectedIndex = 0
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: false, completion: nil)
       
    }

    @IBAction override func showMapsController() {
        let vc = self.storyboard?.instantiateViewController(identifier: "MainTab") as! UITabBarController
        vc.selectedIndex = 2
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: false, completion: nil)
        
    }

    @IBAction override func showInfoController() {
        let vc = self.storyboard?.instantiateViewController(identifier: "MainTab") as! UITabBarController
        vc.selectedIndex = 3
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: false, completion: nil)
        
    }

    @IBAction override func showFavoriteController() {
//        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Favorite") as! Favorite
//        self.navigationController?.pushViewController(controller, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(identifier: "MainTab") as! UITabBarController
        vc.selectedIndex = 1
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: false, completion: nil)
    }
}


