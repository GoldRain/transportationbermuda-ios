//
//  Maps.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-21.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//

import UIKit
//import MapKit

class Maps: BaseVC, UIScrollViewDelegate {
    //MKMapViewDelegate
    

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
  
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = (self as? UIScrollViewDelegate)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
}

/* let locationManager =
  //      CLLocationManager()
    
  //  @IBOutlet weak var mapView: MKMapView!


    override func viewDidLoad() {
        super.viewDidLoad()
   //      locationManager.delegate = self
   //     locationManager.desiredAccuracy = kCLLocationAccuracyBest
   //     locationManager.requestWhenInUseAuthorization()
   //     locationManager.requestLocation()
    
}
        // 1.
  /*      mapView.delegate = self
        
        // 2.
        let hamiltonLocation = CLLocationCoordinate2D(latitude: 32.291436, longitude: -64.786412)
        
            //blue route
        let dockyardLocation = CLLocationCoordinate2D(latitude: 32.326574, longitude: -64.833496)
        
            //pink route
        let lowerferryLocation = CLLocationCoordinate2D(latitude: 32.288235, longitude: -64.780775)
        let hodsdonsLocation = CLLocationCoordinate2D(latitude: 32.284607, longitude: -64.785969)
        let saltkettleLocation = CLLocationCoordinate2D(latitude: 32.284045, longitude: -64.792644)
        let darrellsLocation = CLLocationCoordinate2D(latitude: 32.280848, longitude: -64.796116)
        let belmontLocation = CLLocationCoordinate2D(latitude: 32.277365, longitude: -64.800581)
        let hinsonsLocation = CLLocationCoordinate2D(latitude: 32.282844, longitude: -64.800022)

            //green route
        let watfordbridgeLocation = CLLocationCoordinate2D(latitude: 32.304030, longitude: -64.858824)
        let cavellobayLocation = CLLocationCoordinate2D(latitude: 32.297704, longitude: -64.859959)
        let rockawayLocation = CLLocationCoordinate2D(latitude: 32.263903, longitude: -64.864104)
        

        // 3.
        let hamiltonPlacemark = MKPlacemark(coordinate: hamiltonLocation, addressDictionary: nil)
        let dockyardPlacemark = MKPlacemark(coordinate: dockyardLocation, addressDictionary: nil)
        let hodsdonsPlacemark = MKPlacemark(coordinate: hodsdonsLocation, addressDictionary: nil)
        let lowerferryPlacemark = MKPlacemark(coordinate: lowerferryLocation, addressDictionary: nil)
        let saltkettlePlacemark = MKPlacemark(coordinate: saltkettleLocation, addressDictionary: nil)
        let darrellsPlacemark = MKPlacemark(coordinate: darrellsLocation, addressDictionary: nil)
        let belmontPlacemark = MKPlacemark(coordinate: belmontLocation, addressDictionary: nil)
        let hinsonsPlacemark = MKPlacemark(coordinate: hinsonsLocation, addressDictionary: nil)
        let watfordbridgePlacemark = MKPlacemark(coordinate: watfordbridgeLocation, addressDictionary: nil)
        let cavellobayPlacemark = MKPlacemark(coordinate: cavellobayLocation, addressDictionary: nil)
        let rockawayPlacemark = MKPlacemark(coordinate: rockawayLocation, addressDictionary: nil)

        
        // 4.
        let hamiltonMapItem = MKMapItem(placemark: hamiltonPlacemark)
        let dockyardMapItem = MKMapItem(placemark: dockyardPlacemark)
        let lowerferryMapItem = MKMapItem(placemark: lowerferryPlacemark)
        let watfordbridgeMapItem = MKMapItem(placemark:watfordbridgePlacemark)
        let cavellobayMapItem = MKMapItem(placemark: cavellobayPlacemark)
        let rockawayMapItem = MKMapItem(placemark: rockawayPlacemark)
        let hodsdonsMapItem = MKMapItem(placemark: hodsdonsPlacemark)
        let saltkettleMapItem = MKMapItem(placemark: saltkettlePlacemark)
        let darrellsMapItem = MKMapItem(placemark: darrellsPlacemark)
        let belmontMapItem = MKMapItem(placemark: belmontPlacemark)
        let hinsonsMapItem = MKMapItem(placemark: hinsonsPlacemark)

        // 5.
        let hamiltonAnnotation = MKPointAnnotation()
        hamiltonAnnotation.title = "Hamilton Ferry"
        
        if let location = hamiltonPlacemark.location {
            hamiltonAnnotation.coordinate = location.coordinate
        }
        
        let dockyardAnnotation = MKPointAnnotation()
        dockyardAnnotation.title = "Royal Naval Dockyard"
        
        if let location = dockyardPlacemark.location {
            dockyardAnnotation.coordinate = location.coordinate
        }
        
        let lowerferryAnnotation = MKPointAnnotation()
        lowerferryAnnotation.title = "Lower Ferry"
        
        if let location = lowerferryPlacemark.location {
            lowerferryAnnotation.coordinate = location.coordinate
        }
        
        let hodsdonsAnnotation = MKPointAnnotation()
        hodsdonsAnnotation.title = "Hodsdon's Ferry"
        
        if let location = hodsdonsPlacemark.location {
            hodsdonsAnnotation.coordinate = location.coordinate
        }
        
        let saltkettleAnnotation = MKPointAnnotation()
        saltkettleAnnotation.title = "Salt Kettle"
        
        if let location = saltkettlePlacemark.location {
            saltkettleAnnotation.coordinate = location.coordinate
        }
        
        let darrellsAnnotation = MKPointAnnotation()
        darrellsAnnotation.title = "Darrell's Wharf"
        
        if let location = darrellsPlacemark.location {
            darrellsAnnotation.coordinate = location.coordinate
        }
        
        let belmontAnnotation = MKPointAnnotation()
        belmontAnnotation.title = "Belmont Ferry"
        
        if let location = belmontPlacemark.location {
            belmontAnnotation.coordinate = location.coordinate
        }
        
        let hinsonsAnnotation = MKPointAnnotation()
        hinsonsAnnotation.title = "Hinson's Island"
        
        if let location = hinsonsPlacemark.location {
            hinsonsAnnotation.coordinate = location.coordinate
        }
        let watfordbridgeAnnotation = MKPointAnnotation()
        watfordbridgeAnnotation.title = "Watford Bridge"
        
        if let location = watfordbridgePlacemark.location {
            watfordbridgeAnnotation.coordinate = location.coordinate
        }
        
        let cavellobayAnnotation = MKPointAnnotation()
        cavellobayAnnotation.title = "Cavello Bay"
        
        if let location = cavellobayPlacemark.location {
            cavellobayAnnotation.coordinate = location.coordinate
        }
        
        let rockawayAnnotation = MKPointAnnotation()
        rockawayAnnotation.title = "Rockaway"
        
        if let location = rockawayPlacemark.location {
            rockawayAnnotation.coordinate = location.coordinate
        }
        
        
        
        

        
        // 6.
        self.mapView.showAnnotations([hamiltonAnnotation,dockyardAnnotation, watfordbridgeAnnotation, cavellobayAnnotation, rockawayAnnotation, lowerferryAnnotation, hodsdonsAnnotation, saltkettleAnnotation, darrellsAnnotation, belmontAnnotation, hinsonsAnnotation ], animated: true )
        
        // 7.
        let directionRequest = MKDirections.Request()
        directionRequest.source = hamiltonMapItem
        directionRequest.destination = dockyardMapItem
        directionRequest.transportType = .any
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    
    
  //  func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
    //    let renderer = MKPolylineRenderer(overlay: overlay)
    //    renderer.strokeColor = UIColor.red
    //    renderer.lineWidth = 4.0
        
    //    return renderer
    //}

 */
 


extension Maps : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: (error)")
    }
}
*/


