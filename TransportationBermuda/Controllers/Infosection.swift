//
//  aaaaaViewController.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-16.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//

import UIKit
import MessageUI

class Infosection : BaseVC, UINavigationControllerDelegate {
    
    //creation tableview
    @IBOutlet weak var tableView4: UITableView!

    class SectionData {
        let title: String
        let sentences: [String]
        let whenTapped: ()->()
        var isOpen = false
        
        init (title: String,
              sentences: [String],
              whenTapped: @escaping ()->()) {
            
            self.title = title
            self.sentences = sentences
            self.whenTapped = whenTapped
        }
        
        @objc func wasTapped (_ sender: Any) {
            print("triggered", title)
            whenTapped()
        }
    }
    
    var sectionData: [SectionData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView4.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        sectionData = [
            SectionData(
                title: "About",
                sentences: ["Easy to use, the app gives you all the information you need to get around by ferry in Bermuda.",
                    "The app was developed by myself. We moved to beautiful Bermuda in 2018.", "It is my first app so don't hesite to give me feedback :)"],
                whenTapped: {
                        self.sectionData[0].isOpen.toggle()
                        self.tableView4.reloadData()
                }),
            SectionData(
                title: "Help",
                sentences: ["You can contact directly the Hamilton Ferry Terminal* at 441-295-4506. Service is subject to charge.",
                            "*Mon-Fri 6:30am-20:00, Sat 7:30am-18:00, Sun & Holidays 8:30am-18:00.",
                            "For ferry scheduling information and alert notifications, please visit www.marineandports.bm"],
                whenTapped: {
                    self.sectionData[1].isOpen.toggle()
                    self.tableView4.reloadData()
            }),
            SectionData(
                title: "Feedback",
                sentences: [""],
                whenTapped: {
                    
                    guard MFMailComposeViewController.canSendMail() else { return }
                   
                    let mailController = MFMailComposeViewController()
                    mailController.mailComposeDelegate = self
                    mailController.delegate = self
                    mailController.setToRecipients(["transportationbermuda@gmail.com"])
                    mailController.setSubject("Share your comments with us")
                    //mailController.dismiss(animated: true, completion: nil)
                    self.present(mailController, animated: true)
                
                    if MFMailComposeViewController.canSendMail() {
                        self.present(mailController, animated: true, completion: nil)
                    }

                    func mailComposeController(controller: MFMailComposeViewController,
                                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
                        // Check the result or perform other tasks.
                        
                        // Dismiss the mail compose view controller.
                        controller.dismiss(animated: true, completion: nil)
                    }
                    
                    whenTapped: do {
                        self.sectionData[2].isOpen.toggle()
                        self.tableView4.reloadData()
                    }
                    
            }),
            SectionData(
                title: "Disclaimer",
                sentences: ["The information provided by our mobile application is for general informational purposes only.", "All information on our mobile application is provided in good faith, however we make no representation or warranty of any kind, express or implied, regarding the accuracy, adequacy, validity, reliability, availability or completeness of any information on our mobile application.", "Under no circumstance shall we have any liability to you for any loss or damage of any kind incurred as a result of the use of our mobile application or reliance on any information provided on our mobile application.", "Your use of our mobile application and your reliance on any information on our mobile application is solely at your own risk."],
                whenTapped: {
                        self.sectionData[3].isOpen.toggle()
                        self.tableView4.reloadData()
                   
            }),
            
            SectionData(
                title: "Privacy Policy",
                sentences: [""],
                whenTapped: {
                    guard let url = URL(string: "https://www.termsfeed.com/live/12255e74-ef86-462f-9a1c-90cd2fde91b0") else { return }
                    UIApplication.shared.open(url)
            }),
        ]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addButtonNavBar()
    }
    
}

extension Infosection: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5// sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = sectionData[section]
        let value = data.isOpen ? data.sentences.count : 0
        return value
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionData[section].title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView (_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        print("header", section)
        let data = sectionData[section]
        let header = UIView()
        let label = UILabel()

        //        Copperplate
        label.text = data.title
        label.font = UIFont (name: "Copperplate", size: 36)
//        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.textColor = .darkGray
        label.sizeToFit()
        header.addSubview(label)
        header.backgroundColor = .white
        header.layer.cornerRadius = 5
        header.translatesAutoresizingMaskIntoConstraints = false
  
        let button = UIButton(frame: .zero)
        button.addTarget(sectionData[section], action: #selector(sectionData[section].wasTapped(_:)), for: .touchUpInside)
        header.addSubview(button)
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        print("will display", section)
        guard let label = view.subviews.first as? UILabel else { return }
        label.center.y = view.frame.size.height/2
        label.frame.origin.x = 20
        guard let button = view.subviews[1] as? UIButton else { return }
        button.frame.size = view.frame.size
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView4.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.translatesAutoresizingMaskIntoConstraints = false
        cell.textLabel?.font = UIFont(name: "Cambria", size: 25)
        cell.textLabel?.text = sectionData[indexPath.section].sentences[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.backgroundColor = UIColor.white
        return cell
    }
    
}

extension Infosection: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result:          MFMailComposeResult, error: Error?) {
         if let _ = error {
            self.dismiss(animated: true, completion: nil)
         }
        controller.dismiss(animated: true, completion: nil)

    }

    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {

        self.dismiss(animated: true, completion: nil)
    }
}
