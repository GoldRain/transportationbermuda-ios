//
//  BaseVC.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit
import MBProgressHUD

//MARK:- base view controller: defined common functions
class BaseVC: UIViewController {
    
    var hud: MBProgressHUD?
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // When post local notification to app if app badge number changed, this will receive it and forward to setBadgeCount function.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setBadgeCount),
                                               name: Notification.Name("changedBadgeCount"),
                                               object: nil)
        
        
        // When post local notification to app, this will receive that, and forward setBageCount
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setFCMToken),
                                               name: Notification.Name("FCMToken"),
                                               object: nil)
        
    }

    //MARK:- custom functions
    @objc func setFCMToken() {
        FirebaseAPI.saveMyToken { (isSuccess, msg) in
            print("SaveToken: \(isSuccess), \(msg)")
        }
    }
    
    func addButtonNavBar() {
        
        // This will add envelop when owner's notification exist. And if no, then this will remove envelop icon
        // if new child will added, this will show icon.
        // if one child will removed, this will check all childs, and if some child exist, this will add icon.
        // or if nothing is exist, this will remove icon
        FirebaseAPI.addListenerReports { (isSuccess, data, changeType) in
            
            let temp = data as! [ReportModel]
            print("temp.count::::", temp.count)
            
            if isSuccess, temp.count > 0 {
                
                if changeType == .added {
                    self.showRightButton()
                } else if changeType == .removed {
                    
                    FirebaseAPI.getAllReports { [self] (isSuccess, data) in
                        if isSuccess {
                            let temp = data as! [ReportModel]
                            
                            if temp.count > 0 {
                                self.showRightButton()
                            } else {
                                self.navigationItem.rightBarButtonItem = nil
                                self.navigationItem.rightBarButtonItem?.removeBadge()
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func showRightButton() {
        
        //right barbutton
        let butInfo = UIButton(type: .custom)
        butInfo.setImage(UIImage (named: "ic_letter"), for: .normal)
        butInfo.addTarget(self, action: #selector(self.addTappedInfo), for: .touchUpInside)
        butInfo.imageEdgeInsets = UIEdgeInsets(top: 3, left: 6, bottom: 3, right: 0)
        butInfo.tintColor = CONSTANT.COLOR_PRIMARY
        let barButtonItemInfo = UIBarButtonItem(customView: butInfo)
        barButtonItemInfo.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemInfo.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        self.navigationItem.rightBarButtonItem = barButtonItemInfo
        
        self.setBadgeCount()
    }
    
    @objc func addTappedInfo(isConfirm: Bool) {
        gotoNavVC("ReportsVC")
    }
    
    // set badge count if that exist, or if no, this will be removed.
    @objc func setBadgeCount() {
        let rightBarButtons = self.navigationItem.rightBarButtonItems
        let lastBarButton = rightBarButtons?.last
        
        let point = CGPoint(x: -42, y: 0)
        lastBarButton?.addBadge(number: UIApplication.shared.applicationIconBadgeNumber, withOffset: point)
    }
    
    
    //push new viewcontroller of same navigation controller
    func gotoNavVC (_ nameVC: String, _ animated: Bool = true) {
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        self.navigationController?.pushViewController(toVC!, animated: animated)
    }
    
    // pop one viewcontroller from navigation controller.
    func doDismiss(_ animated: Bool = true){
        self.navigationController?.popViewController(animated: animated)
    }
    
    // pop to root viewcontroller from navigation controller.
    func doRootDismiss(_ animated: Bool = true){
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
}

//MARK:- alert, progressHUD
extension BaseVC {
    
    //this will make alert dialog using titile, message body, positive button, negative button
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        DispatchQueue.main.async(execute:  {
            self.present(alert, animated: true, completion: nil)
        })
    }

    //this will make alert dialog using only message body
    func showError(_ message: String!) {
        showAlertDialog(title: "", message: message, positive:"OK", negative: nil)
    }

    func showSuccess(_ message: String!) {
        showAlertDialog(title: "", message: message, positive: "OK", negative: nil)
    }

    internal func showAlert(title: String?, message: String?, positive: String, negative: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if negative != nil {
            let noAction = UIAlertAction(title: negative, style: .destructive, handler: { (action: UIAlertAction) in

            })
            alertController.addAction(noAction)
        }

        let yesAction = UIAlertAction(title: positive, style: .default, handler: { (action: UIAlertAction) in
            if okClosure != nil {
                okClosure!()
            }
        })
        alertController.addAction(yesAction)


        self.present(alertController, animated: true, completion: nil)
    }
    

    func showAlert(_ message: String!) {
        showAlertDialog(title: R.string.APP_NAME, message: message, positive: "OK", negative: nil)
    }
    

    //MARK:- progressHUD function
    //this will make progressHUD with label
    func showLoadingView(label: String = "") {

        let window = UIApplication.shared.key!.rootViewController

        if window != nil {
            hud = MBProgressHUD.showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        
        if label != "" {
            hud!.label.text = label;
        }
        
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.tintColor = UIColor.gray
        hud!.contentColor = CONSTANT.COLOR_PRIMARY
    }
    
    //this will make progressHUD with viewcontroller and label
    func showLoadingView(vc: UIViewController, label: String = "") {
        
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        
        if label != "" {
            hud!.label.text = label;
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.tintColor = UIColor.gray
        hud!.contentColor = CONSTANT.COLOR_PRIMARY
    }
    
    func showProgressHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
    
        let hud = MBProgressHUD .showAdded(to:view, animated: true)
        hud.mode = mode
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = CONSTANT.COLOR_PRIMARY
        return hud
    }
    
    //this will hide progressHUD
    func hideLoadingView() {
       if let hud = hud {
           hud.hide(animated: true)
       }
    }
}
