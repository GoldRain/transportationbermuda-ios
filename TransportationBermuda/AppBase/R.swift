//
//  R.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//


//MARK:- String resource for app
struct R {
    struct string {

        static let APP_NAME                     = "Transportation Bermuda"
        static let BUT_OK                       = "OK"
        static let BUT_CANCEL                   = "Cancel"
        static let REPORT_TITLE                 = "Please add title."
        static let REPORT_CONTENT               = "Please add content."
        static let REPORT_SUCCESS               = "Your report sent successfully, please wait."
        static let REPORT_ERROR                 = "Something went wrong!"
        
        //messages
        static let REPORT_SENDING               = "Sending now..."
        static let REPORT_LOADING               = "Loading now..."
        static let NOTI_BODY                    = "Someone sent to you new report."
        
    }
}
