//
//  AppDelegate.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-06.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//
//
import UIKit
import Firebase
import IQKeyboardManagerSwift
import UserNotifications
import SwiftMessages
import GoogleSignIn
import FBSDKLoginKit



var myUDID = String()
var deviceTokenString = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      // ...
      if let error = error {
        // ...
        return
      }

      guard let authentication = user.authentication else { return }
      let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                        accessToken: authentication.accessToken)
      // ...
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //MARK:- firebase configuration
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        IQKeyboardManager.shared.enable = true
        
        //MARK:- notification register
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        
        //MARK:- get FCM token
        Messaging.messaging().delegate = self
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                
                NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil)
                print("FCM registration token: \(token)")
                deviceTokenString = token
            }
        }
        
        //get my device ID
        if let deviceID = UIDevice.current.identifierForVendor?.uuidString {
            myUDID = deviceID
        }
        
        
        //MARK:- Navigationbar Appearance
        let buttonAppearance = UIBarButtonItemAppearance()
        buttonAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.clear]
        let appearance = UINavigationBarAppearance()
        appearance.buttonAppearance = buttonAppearance
        
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UIBarButtonItem.appearance().tintColor = UIColor(named: "PrimaryColor")
        

        return true
    }
    
    //google Login
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
      -> Bool {
      return GIDSignIn.sharedInstance().handle(url)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}


//MARK:- Notification Center delegate
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    //MARK:- When receive notification on forground state
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // parse notification data
        let userInfo = notification.request.content.userInfo
        
        let aps             = userInfo["aps"] as? [AnyHashable : Any]
        let badgeCount      = aps!["badge"] as? Int ?? 0
        let alertMessage    = aps!["alert"] as? [AnyHashable : Any]
        let bodyMessage     = alertMessage!["body"] as! String
        let titleMessage    = alertMessage!["title"] as! String
        
        if badgeCount > 0 {
            UIApplication.shared.applicationIconBadgeNumber = badgeCount
        } else {
            UIApplication.shared.applicationIconBadgeNumber += 1
        }
        
        // When change app badge number, it will post local notification to app.
        NotificationCenter.default.post(name: Notification.Name("changedBadgeCount"), object: nil)

        //make and show in app messageview
        let view: MessageView
        view = try! SwiftMessages.viewFromNib()
        let icon = UIImage(named: "AppIcon180")!.resize(to: CGSize(width: 35, height: 35))!.withRoundedCorners(radius: 5)
        
        view.configureContent(title: titleMessage, body: bodyMessage, iconImage: icon, iconText: nil, buttonImage: nil, buttonTitle: "OK", buttonTapHandler: { _ in
            SwiftMessages.hide()
            UIApplication.shared.applicationIconBadgeNumber -= 1
        })
        
        view.configureTheme(backgroundColor: CONSTANT.COLOR_PRIMARY!, foregroundColor: UIColor.white, iconImage: icon, iconText: nil)
//        view.button?.setImage(Icon.errorSubtle.image, for: .normal)
        view.button?.setTitle("OK", for: .normal)
        view.button?.backgroundColor = UIColor.white ///UIColor.clear
        view.button?.tintColor = CONSTANT.COLOR_PRIMARY!
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.duration = .forever //.seconds(seconds: 5)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        config.shouldAutorotate = true
        config.interactiveHide = true
        config.preferredStatusBarStyle = .lightContent
        
        SwiftMessages.show(config: config, view: view)
        
        completionHandler([])
    }
}


//MARK:- Messeging delegate:
extension AppDelegate : MessagingDelegate {
    //MARK:- FCM token
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Messaging.messaging().subscribe(toTopic: "/topics/all")
        deviceTokenString = fcmToken!
        
        // When get FCM token, it will post local notification to app.
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
}

