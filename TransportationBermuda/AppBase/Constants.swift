//
//  Constants.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import UIKit
import Foundation

// MARK:- App main constants
class CONSTANT {
    
    static let TOKENS                      = "tokens"
    static let USER_UDID                   = "user_udid"
    static let USER_TOKEN                  = "user_token"
    static let BADGE_COUNT                 = "badge_count"
    
    
    static let REPORTS                     = "reports"
//    static let DOC_ROWID                   = "doc_rowid"
    static let REPORT_TIMESTAMP            = "report_timestamp"
    static let REPORT_TITLE                = "report_title"
    static let REPORT_CONTENT              = "report_content"
    static let REPORT_                     = "report_users"
    

    //Color
    static let COLOR_PRIMARY               = UIColor(named: "PrimaryColor")
}
