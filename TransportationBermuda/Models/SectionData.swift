//
//   SectionData.swift
//  Ferry Schedule
//
//  Created by Maude Chiasson on 2019-12-15.
//  Copyright © 2019 Maude Chiasson. All rights reserved.
//


import Foundation
import UIKit

class FerrySchedule: Codable {
    var routes: [RouteSchedule]
    func connect() {
        
        let isWinter = Date().isWinter
        self.routes = self.routes.filter { $0.isWinterSchedule == isWinter }
        
        for route in self.routes {
            route.schedule = self
            route.connect()
        }
    }
    
    static var schedule: FerrySchedule = {
        let url = Bundle.main.url(forResource: "SectionData", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        return try! JSONDecoder().decode(FerrySchedule.self, from: data)
    }()
}

class RouteSchedule: Codable {
    var schedule: FerrySchedule?
    func connect() {
        for station in self.stations {
            station.route = self
            station.connect()
        }
    }
    let title: String
    let color: [CGFloat]
    let isWinterSchedule: Bool
    let stations: [StationSchedule]
    let headerImageName: String
    
    var headerColor: UIColor {
        if color.count >= 3 {
            return UIColor(red: color[0], green: color[1], blue: color[2], alpha: 1)
        }
        return .gray
    }
}

class StationSchedule: Codable {
    func connect() {
        for day in self.days {
            day.stationSchedule = self
            day.connect()
        }
    }
    var route: RouteSchedule?
    let title: String
    let days : [DaySchedule]
}

class DaySchedule: Codable {
    func connect() {
        for routeTime in self.routeTimes ?? [] {
            routeTime.daySchedule = self
        }
    }
    var stationSchedule: StationSchedule?
    let times : [String]
    let title: String
    let routeTimes: [RouteTimes]?
    
    static func getNextTimeIndex(times: [String]) -> Int? {
        let now = Date().toString("HH:mm")
        let nowElements = now.components(separatedBy: ":")
        let nowH = Int(nowElements[0]) ?? 0
        let nowM = Int(nowElements[1]) ?? 0
        
        // split now to hour and min, split every time in data times to hour and min. compare hour -> greater -> next time
        // hour equal -> compare min
        for i in 0 ..< times.count {
            let item = times[i]
            let itemElements = item.components(separatedBy: ":")
            guard let h = Int(itemElements[0]),
                let m = Int(itemElements[1]) else { continue }
            // 15:40 -> 16:00
            if h > nowH {
                return i
            } else if h == nowH && m > nowM {
                return i
            }
        }
        
        return nil
    }
}

class RouteTimes: Codable {
    var daySchedule: DaySchedule?
    let visibleTime: String
    let stops: [StopAndTime]
    
    class StopAndTime: Codable {
        let stop: String
        let time: String
    }
}

