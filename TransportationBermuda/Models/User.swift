//
//  User.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-17.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import Foundation
import Firebase

let UserId = "id"
let UserEmail = "email"
let UserFavRoutes = "favRoutes"
let UserFavStations = "favStations"

class User {
    var id: String?
    var email: String?
    var favRoutes: [String]?
    var favStations: [String]?
    var isLoggedIn: Bool {
        if self.id != nil && Auth.auth().currentUser != nil {
            return true
        } else {
            return false
        }
    }
    
    static let currentUser: User = {
        let instance = User()
        return instance
    }()
    
    init() {
        self.setUp()
    }
    
    func setUp() {
        self.id = UserDefaults.standard.string(forKey: UserId)
        self.email = UserDefaults.standard.string(forKey: UserEmail)
        self.favRoutes = UserDefaults.standard.value(forKey: UserFavRoutes) as? [String]
        self.favStations = UserDefaults.standard.value(forKey: UserFavStations) as? [String]
    }
    
    func saveUserInfo(userInfo: [String: Any]) {
        if let id = userInfo[UserId] as? String {
            self.id = id
            UserDefaults.standard.set(self.id, forKey: UserId)
        }
        
        if let email = userInfo[UserEmail] as? String {
            self.email = email
            UserDefaults.standard.set(self.email, forKey: UserEmail)
        }
        
        UserDefaults.standard.setValue(true, forKey: "loginStatus")
    }
    
    func saveFavRoutes(favs: [String]?) {
        self.favRoutes = favs
        UserDefaults.standard.set(self.favRoutes, forKey: UserFavRoutes)
    }
    
    func saveFavStations(stations: [String]?) {
        self.favStations = stations
        UserDefaults.standard.set(self.favStations, forKey: UserFavStations)
    }
    
    func logOut() {
        self.id = nil
        self.email = nil
        self.favRoutes = nil
        self.favStations = nil
        
        UserDefaults.standard.set(nil, forKey: UserId)
        UserDefaults.standard.set(nil, forKey: UserEmail)
        UserDefaults.standard.set(nil, forKey: UserFavRoutes)
        UserDefaults.standard.set(nil, forKey: UserFavStations)
    }
}
