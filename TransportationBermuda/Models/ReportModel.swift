//
//  ReportModel.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

class ReportModel {
    
    var row_id              = ""
    var report_title        = ""
    var report_content      = ""
    var report_timestamp:Int64    = 0
    
    init(row_id: String, report_title: String, report_content: String, report_timestamp: Int64) {
        self.row_id             = row_id
        self.report_title       = report_title
        self.report_content     = report_content
        self.report_timestamp   = report_timestamp
    }
}
