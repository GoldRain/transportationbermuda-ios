//
//  NetworkServices.swift
//  TransportationBermuda
//
//  Created by Maude Chiasson on 2020-01-23.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
////
//
import Foundation
import Firebase

class NetworkServices {
    
    static func saveUserPreference(route: Int, station: Int, complition: @escaping (_ isSucceed: Bool, _ error: String?) -> Void) {
        
        let docref = Firestore.firestore().collection("users").document("\(User.currentUser.id!)").collection("preferences").document()
        
        var prefDic = [String: Any]()
        prefDic["route"] = route
        prefDic["station"] = station

        docref.setData(prefDic) { (error) in
            if error != nil {
                complition(false, "Error")
            } else {
                print(docref.documentID)
                complition(true, docref.documentID)
            }
        }
    }
}
