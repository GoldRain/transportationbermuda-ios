//
//  FirebaseAPI.swift
//  TransportationBermuda
//
//  Created by Rapunzel on 11/23/20.
//  Copyright © 2020 Maude Chiasson. All rights reserved.
//

import Firebase
import SwiftyJSON
import FirebaseFirestore


class FirebaseAPI {
    
    //MARK:-    set variable data
    static let db                       = Firestore.firestore()
    static let reportsCollection        = db.collection(CONSTANT.REPORTS)
    static let tokenCollection          = db.collection(CONSTANT.TOKENS)
    
    //MARK:-    parse Report data
    static func parseReporteData(_ document: QueryDocumentSnapshot) -> ReportModel {
        
        let data        =  document.data()
        let doc_rowid   = document.documentID
        let timeTemp            = data[CONSTANT.REPORT_TIMESTAMP] as? Int64 ?? Int64(NSDate().timeIntervalSince1970)
        let report_title        = data[CONSTANT.REPORT_TITLE] as? String ?? ""
        let report_content      = data[CONSTANT.REPORT_CONTENT] as? String ?? ""
        
        let one = ReportModel(row_id: doc_rowid, report_title: report_title, report_content: report_content, report_timestamp: timeTemp)
        
        return one
    }
    
    //MARK:-    get Report data
    static func getAllReports(completion: @escaping (_ state: Bool, _ result: Any) -> () ) {
        
        reportsCollection.getDocuments() { (querySnapshot, err) in
            if let err = err {
                completion(false, err)
            } else {
                
                var result = [ReportModel]()
                
                guard let snap = querySnapshot else { return }
                for document in snap.documents {
                    
                    let one = parseReporteData(document)
                    result.append(one)
                }
                
                completion(true, result)
            }
        }
    }
    
    //MARK:- add Listener to report collection of firestore
    static func addListenerReports(completion: @escaping (_ state: Bool, _ result: Any, _ changedType: DocumentChangeType) -> () ) {
        
        reportsCollection.addSnapshotListener { querySnapshot, error in
            
            var changedType = DocumentChangeType.added
            var result = [ReportModel]()
            
            if let err = error {
                completion(false, err, changedType)
            }
            else {
                guard let snapshot = querySnapshot else {return}
                
                let source = snapshot.metadata.hasPendingWrites ? "Local" : "Server"
                print("source:::::::", source)
                snapshot.documentChanges.forEach { diff in
                    
                    changedType = diff.type
                    
                    let one = parseReporteData(diff.document)
                    result.append(one)
                }
                
                completion(true, result, changedType)
            }
        }
    }
    
    // MARK:- save My Report
    static func saveMyToken(completion: @escaping (_ status: Bool, _ result: String) -> ()) {
        
        tokenCollection.whereField(CONSTANT.USER_UDID, isEqualTo: myUDID)
            .getDocuments() { (querySnapshot, err) in
            if let err = err {
                completion(false, err.localizedDescription)
            }
            else {
                
                guard let snap = querySnapshot else { return }
                let data = [
                        CONSTANT.USER_UDID      : myUDID,
                        CONSTANT.USER_TOKEN     : deviceTokenString,
                    ] as [String:Any]
                
                if snap.count > 0 {
                    
                    let temp = snap.documents[0]
                    let doc_rowid   = temp.documentID
                    
                    // update my FCM token to token collection of firestore.
                    tokenCollection.document(doc_rowid).updateData(data){ err in
                        if let err = err {
                            completion(false, err.localizedDescription)
                        } else {
                            completion(true, "")
                        }
                    }
                    
                } else {
                    var ref: DocumentReference? = nil
                    ref = tokenCollection.addDocument(data: data) { err in
                        if let err = err {
                            completion(false, err.localizedDescription)
                        } else {
                            completion(true, ref!.documentID)
                        }
                    }
                }
            }
        }
    }
    
}

